var hljs = require('highlight.js/lib/highlight.js');

hljs.registerLanguage('bash', require('highlight.js/lib/languages/bash'));
hljs.registerLanguage('css', require('highlight.js/lib/languages/css'));
hljs.registerLanguage('markdown', require('highlight.js/lib/languages/markdown'));
hljs.registerLanguage('diff', require('highlight.js/lib/languages/diff'));
// hljs.registerLanguage('go', require('highlight.js/lib/languages/go'));
hljs.registerLanguage('javascript', require('highlight.js/lib/languages/javascript'));
hljs.registerLanguage('json', require('highlight.js/lib/languages/json'));
hljs.registerLanguage('yaml', require('highlight.js/lib/languages/yaml'));
hljs.registerLanguage('xml', require('highlight.js/lib/languages/xml'));
hljs.registerLanguage('html', require('highlight.js/lib/languages/handlebars'));

/*
Go highlighting originally from
https://raw.githubusercontent.com/isagalaev/highlight.js/master/src/languages/go.js

Language: Go
Author: Stephan Kountso aka StepLg <steplg@gmail.com>
Contributors: Evgeny Stepanischev <imbolk@gmail.com>
Description: Google go language (golang). For info about language see http://golang.org/
Category: system

Extended by Bud Parr for special highlighting in gohugoioTheme
for Hugo documentation,
see https://github.com/gohugoio/gohugoioTheme/commit/0675875b348709e82a2205248ad668e54b3c7652#diff-42e828679c7b94b12d8090779f58df7d
and https://github.com/gohugoio/gohugoioTheme/commit/966215fbb2ec306f3bc550668963bcb52f498e22#diff-42e828679c7b94b12d8090779f58df7d
*/

hljs.registerLanguage("go", function(hljs) {
  var GO_KEYWORDS = {
    keyword:
      'code output note warning ' +
      'break default func interface select case map struct chan else goto package switch ' +
      'const fallthrough if range ' +
      'end ' +
      'type continue for import return var go defer ' +
      'bool byte complex64 complex128 float32 float64 int8 int16 int32 int64 string uint8 ' +
      'uint16 uint32 uint64 int uint uintptr rune ' +
      'id autoplay Get',
    literal:
       'file download copy ' +
       'true false iota nil ' +
       'Pages with',
    built_in:
      'append cap close complex ' +
      'highlight ' +
      'copy imag len make new panic print println real recover delete ' +
      'Site Data tweet speakerdeck youtube ref relref vimeo instagram gist figure innershortcode'
  };
  return {
    aliases: ['golang', 'hugo'],
    keywords: GO_KEYWORDS,
    illegal: '</',
    contains: [
      hljs.C_LINE_COMMENT_MODE,
      hljs.C_BLOCK_COMMENT_MODE,
      {
        className: 'string',
        variants: [
          hljs.QUOTE_STRING_MODE,
          {begin: '\'', end: '[^\\\\]\''},
          {begin: '`', end: '`'},
        ]
      },
      {
        className: 'number',
        variants: [
          {begin: hljs.C_NUMBER_RE + '[dflsi]', relevance: 1},
          hljs.C_NUMBER_MODE
        ]
      },
      {
        begin: /:=/ // relevance booster
      },
      {
        className: 'function',
        beginKeywords: 'func', end: /\s*\{/, excludeEnd: true,
        contains: [
          hljs.TITLE_MODE,
          {
            className: 'params',
            begin: /\(/, end: /\)/,
            keywords: GO_KEYWORDS,
            illegal: /["']/
          }
        ]
      }
    ]
  }
});

hljs.initHighlightingOnLoad();
